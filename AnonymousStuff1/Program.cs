﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnonymousStuff1
{
    class Program
    {
        private static List<HouseSomething> listoffilteredtemplist;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //here is a standard type usage

            //note that the class definition is already present. 

            HouseSomething temphouse = new HouseSomething();
            int a = temphouse.returnthetemp();

            //8 to 10 lines.
            Console.WriteLine("predefined type - value in a is {0}", a);

            //this will be an anonymous type code
            //it tries to do the same thing as the above HouseSomething class



            var temphouse2 = new
            {
                temp = 10,
                temp2 = "hello toni",
                temp3 = 100.65,
                temp4 = 'a'
                //var temp = 10;
            };
            a = temphouse2.temp;

            //total effort is like 5 lines.
            Console.WriteLine("anonymous type - value in a is {0}", a);

            Console.WriteLine("value in temp is {0} temp 2 is {1} temp 3 {2} temp 4 {3} ", 
                temphouse2.temp,temphouse2.temp2,temphouse2.temp3,temphouse2.temp4);


            //I would like to show you some Linq queries.

            //manually create a list.
            //but remeber that usually the list is going to come from a database.

            List<HouseSomething> houselist = new List<HouseSomething>();

            //as of now, the list is empty

            //lets add some items to the list.

            //create and add. 

            HouseSomething temp1 = new HouseSomething();
            temp1.setthetemp(10);
            HouseSomething temp2 = new HouseSomething();
            temp2.setthetemp(20);
            HouseSomething temp3 = new HouseSomething();
            temp3.setthetemp(30);
            HouseSomething temp4 = new HouseSomething();
            temp4.setthetemp(40);
            HouseSomething temp5 = new HouseSomething();
            temp5.setthetemp(50);

            houselist.Add(temp1);
            houselist.Add(temp2);
            houselist.Add(temp3);
            houselist.Add(temp4);
            houselist.Add(temp5);

            for(int i=0;i<5;i++)
            {
                Console.WriteLine("original list - i is {0} and value of temp is {1}", i, houselist[i].returnthetemp());
            }

            //at this point, i have a list

            //we are going to use linq queries which obviously involves using 
            //some lambda stuff and of course, anonymous types

            var listoffilteredtemp =
                from t in houselist
                select t;

            //convert enumerable type to list type
            listoffilteredtemplist = listoffilteredtemp.ToList();
            //from user in Users select user.LastName+", "+user.FirstName

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("filtered list - i is {0} and value of temp is {1}", i, listoffilteredtemplist[i].returnthetemp());
            }









            Console.ReadLine();
        }
    }

    //this is a regular type

    public class HouseSomething
    {
        public HouseSomething()
        {
            temp = 10;
        }

        public int temp;

        public int returnthetemp()
        {
            return temp;
        }

        public void setthetemp(int a)
        {
            temp = a;
        }
    }
}

/*
 *      
 */
